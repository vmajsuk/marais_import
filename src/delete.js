import { get, del } from './schemaInterface';

/*
Принимает название модели, которую нужно очистить, и удаляет из этой модели все
записи. В случае ошибки повторяет запрос до тех пор, пока он не сработает.
 */
export function deleteRecords(modelName) {
    return get(modelName, {
        limit: -1
    })
        .then(response => response.results)
        .then(records => records.forEach(record => del(modelName + '/' + record.id)));
}
