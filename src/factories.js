import { types } from './config';

/*
Используемые параметры: description, price, productname, reference, season, slug
Загружаемые параметры: active, delivery, description, manufacturer_id,
    manufacturer_name, name, price, runway, season, sku, slug, stock_tracking,
    variable
 */
export function createProduct(prestaProduct) {
    return {
        active: true,
        delivery: 'shipment',
        runway: false,
        stock_tracking: true,
        variable: true,

        description: prestaProduct.description,
        manufacturer_id: prestaProduct.manufacturer_id,
        manufacturer_name: prestaProduct.manufacturer_name,
        name: prestaProduct.productname,
        price: prestaProduct.price,
        season: prestaProduct.season,
        sku: prestaProduct.reference,
        slug: prestaProduct.slug
    };
}

/*
Используемые параметры: description, name, slug
Загружаемые параметры: active, description, name, navigation, parent_id, slug, type
 */
export function createCategory(prestaCategory) {
    return {
        active: true,
        type: types.category,

        description: prestaCategory.description,
        name: prestaCategory.name,
        navigation: false /* написать нормально */,
        parent_id: null /* написать нормально */,
        slug: prestaCategory.slug,
    };
}

/*
Используемые параметры: description, is_active, manufacturer, meta_keywords,
    name, short_description, slug
Загружаемые параметры: active, description, meta_description, meta_keywords, name,
    navigation, parent_id, slug, type
 */
export function createManufacturer(prestaManufacturer, brandsCategoryId) {
    return {
        navigation: true,

        active: prestaManufacturer.is_active,
        description: prestaManufacturer.description,
        meta_description: prestaManufacturer.short_description,
        meta_keywords: prestaManufacturer.meta_keywords,
        name: prestaManufacturer.name,
        parent_id: brandsCategoryId,
        slug: prestaManufacturer.name.toLowerCase().split(' ').join('-'),
        type: types.manufacturer,
    };
}
