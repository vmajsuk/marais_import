import Promise from 'bluebird';

export function ensureSuccess(action, msg) {
    return action()
        .then(res => {
            console.log(msg, 'success');
            return res;
        })
        .catch(err => {
            console.log(msg, 'error', err);
            return ensureSuccess(action);
        });
}

export function queue(action, currentIndex, finishIndex) {
    return currentIndex === finishIndex
        ? Promise.resolve()
        : action()
            .then(() => queue(action, currentIndex + 1, finishIndex));
}

/*
format => http://marais.ru/img/p/2/0/3/5/2035.jpg
  */
export function createImageUrl(id) {
    return 'http://marais.ru/img/p/' + id.split('').join('/') + '/' + String(id) + '.jpg';

    // format => http://mcdn.react-store.com/2035.jpg
    //return 'http://mcdn.react-store.com/' + id + '.jpg';
}