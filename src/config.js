export const
    clientId = 'insoftmarais',
    tokens = {
        'explorer-store': 'EI7kVUmPTpltNgSunDxWtS7z6LmYwNED',
        'marais-test': 'Iwb7XfgdPg2hl2Nqi1clmVXptkIWdYrz',
        'maraisru': '9JUaVyjA40H6aiwmK9SQJ4CvvTaywQD9',
        'marais-live': 'UecFm4gZtulU2BONH3xK4swGJxBqOUDk',
        'marais-temp': 'ioJnGvZqvZ0eMsYLz6atFH4vzzPkYvBr',
        'insoftmarais': 'g6yLVCgHu9SEXd1wGbJdPODRKbFlxPe3'
    },
    fileNames = {
        categories: './data/categories.js.csv',
        manufacturers: './data/manufacturers.csv',
        products: './data/products.csv',
        productsSizes: './data/products-sizes.csv',
        productsImages: './data/product-image-wide-position.csv',
        categoryProductsAssociations: './data/category-products.csv',

        userDataPath: './presta_to_schema_data_maps',
        categoriesPath: 'categories.js-id-to-schema-id.json',
        manufacturersPath: 'manufacturers-id-to-schema-id.json',
        productsPath: 'products-id-to-schema-id.json',
        imagesPath: 'images-id-to-schema-file.json'
    },
    types = {
        category: 'Category',
        manufacturer: 'Manufacturer'
    },
    brandsCategoryName = 'Бренды',
    chunkSize = 100,
    notNavigatedCategories = [
        'Новогодняя акция', 'Pre-Sale', 'Sale',
        'популярное', 'аксессуары', 'одежда', 'glossary'
    ];