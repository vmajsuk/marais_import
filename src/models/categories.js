import { put } from '../schemaInterface';

export function customizeCategoriesModel() {
    return put('/:models/com.categories', {
        fields: {
            type: {
                type: 'string',
                label: 'Type'
            }
        }
    })
}