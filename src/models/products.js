import { post, put } from '../schemaInterface'

function addSizesField() {
    return put('/:models/products/', {
        fields: {
            sizes: {
                fields: {
                    id: {
                        type: "objectid",
                        auto: true
                    },
                    name: {
                        type: "string",
                        required: true
                    },
                    stock_level: {
                        type: "int"
                    }
                },
                label: "Product Sizes",
                type: "array",
                value_type: "object"
            }
        }

    })
}

function addIsReservationAvailableField() {
    return put('/:models/products/', {
        fields: {
            is_reservation_available: {
                type: 'bool',
                label: 'Reservation'
            }
        }
    })
}

function addRunwayField() {
    return put('/:models/products/', {
        fields: {
            runway: {
                type: 'bool',
                label: 'Runway'
            }
        }
    })
}

function addSeasonField() {
    return put('/:models/products/', {
        fields: {
            season: {
                type: 'string',
                label: 'Season'
            }
        }
    })
}

function addRecommendedProductsField() {
    return put('/:models/products/', {
        fields: {
            recommended_products: {
                type: "array",
                value_type: "object",
                fields: {
                    product_id: {
                        type: "objectid"
                    },
                    product: {
                        type: "link",
                        model: "products",
                        key: "product_id"
                    }
                }
            }
        }
    })
}

function addUpdateStockTrigger() {
    return post('/:models/products/fields/stock/triggers', {
        id: "update_product_size_stock_level",
        label: "Update Product Size Stock Level",
        events: [
            "post.result",
            "delete.result"
        ],
        conditions: {
            variant_id: {
                $ne: null
            }
        },
        include: {
            stock_level: {
                url: "/products:stock/:group",
                params: {
                    where: {
                        "variant_id": "variant_id"
                    }
                },
                data: {
                    value: {
                        $sum: "quantity"
                    }
                }
            }
        },
        actions: [
            {
                type: "request",
                method: "put",
                url: "/products/{parent_id}/sizes/{variant_id}",
                params: {
                    stock_level: "stock_level.value"
                }
            }
        ]
    });
}

function addCreateVariantTrigger() {
    return post('/:models/products/fields/variants/triggers', {
        "id": "create_product_size",
        "label": "Create Product Size",
        "events": [
            "post.result"
        ],
        "actions": [
            {
                "type": "request",
                "method": "post",
                "url": "/products/{parent_id}/sizes",
                "params": {
                    "variant_id": "id",
                    "name": "name",
                    "id": "id"
                },
                "data": {
                    "stock_level": 0
                }
            }
        ]
    });
}

function addDeleteVariantTrigger() {
    return post('/:models/products/fields/variants/triggers', {
        "id": "delete_product_size",
        "label": "Delete Product Size",
        "events": [
            "delete.result"
        ],
        "actions": [
            {
                "type": "request",
                "method": "delete",
                "url": "/products/{parent_id}/sizes/{id}"
            }
        ]
    });
}

export function customizeProductsModel() {
    return Promise.all([
        addSizesField(),
        addIsReservationAvailableField(),
        addRunwayField(),
        addSeasonField(),
        addRecommendedProductsField(),
        addUpdateStockTrigger(),
        addCreateVariantTrigger(),
        addDeleteVariantTrigger()
    ]);
}