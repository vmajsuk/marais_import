import { put } from '../schemaInterface';
import { clientId } from '../config';

export function customizeCategoriesProductsModel() {
    return put('/:models/' + clientId + '.com.categories:products', {
        fields:{
            product:{
                type: 'link',
                model: 'products',
                key: 'product_id'
            },
            parent:{
                type: 'link',
                model: 'categories',
                key: 'parent_id'
            }
        }
    });
}