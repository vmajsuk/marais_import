import { post } from '../schemaInterface';

export function createMonitorsModel() {
    return post('/:models', {
        api: 'com',
        id: 'com.monitors',
        label: 'Monitor',
        plural: 'Monitors',
        name: 'monitors',
        fields: {
            sku: {
                type: 'string',
                required: true
            },
            email: {
                type: 'string',
                required: true
            },
            size: {
                type: 'string',
                required: true
            }
        }
    })
}