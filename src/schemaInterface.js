import { ensureSuccess } from './utils';
import { clientId, tokens } from './config';
import schema from 'schema-client';

const
    clientKey = tokens[clientId],
    client = new schema.Client(clientId, clientKey);

export function get(path, params, isSuccessEnsuring = true) {
    return isSuccessEnsuring
        ? ensureSuccess(() => client.get(path, params), 'get' + path)
        : client.get(path, params);
}

export function post(path, params, isSuccessEnsuring = true) {
    return isSuccessEnsuring
        ? ensureSuccess(() => client.post(path, params), 'post' + path)
        : client.post(path, params);
}

export function put(path, params, isSuccessEnsuring = true) {
    return isSuccessEnsuring
        ? ensureSuccess(() => client.put(path, params), 'put' + path)
        : client.put(path, params);
}

export function del(path, params, isSuccessEnsuring = true) {
    return isSuccessEnsuring
        ? ensureSuccess(() => client.delete(path, params), 'delete' + path)
        : client.delete(path, params);
}