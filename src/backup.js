import { get } from './schemaInterface';
import fs from 'fs';

export function createCategoriesBackup() {
    get('categories', {
        limit: -1
    })
        .then(response => response.results)
        .then(categories => {
            !fs.existsSync('./backups') && fs.mkdirSync('backups');

            console.log('Started creating categories backup');
            fs.createWriteStream('./backups/categories.json', JSON.stringify(categories))
                .on('end', () => console.log('Categories backup created'));
        })
}

export function createProductsBackup() {
    get('products', {
        limit: -1
    })
        .then(response => response.results)
        .then(products => {
            !fs.existsSync('./backups') && fs.mkdirSync('backups');

            console.log('Started creating products backup');
            fs.createWriteStream('./backups/products.json', JSON.stringify(products))
                .on('end', () => console.log('Products backup created'));
        })
}