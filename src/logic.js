// import csv from 'csv-parser';
// import schema from 'schema-client';
// import fs from 'fs';
// import { customizeProductsModel } from './models/products';
// import { customizeCategoriesModel } from './models/categories';
// import { createMonitorsModel } from './models/monitors';
// import { customizeCategoriesProductsModel } from './models/categories.products';
//
// import Promise from 'bluebird';
// import ProgressBar from 'progress';
// import { createProduct, createCategory } from './factories';
// import { get, post, put, del } from './schemaInterface';
// import { queue } from './utils';
// import {
//     tokens,
//     fileNames,
//     chunkSize
// } from './config';



// import fs from 'fs';
// import csv from 'csv-parser';
// import { createProduct } from './factories';
// import { post } from './schemaInterface';
// fs.createReadStream('./data/products.csv')
//     .pipe(csv())
//     .on('data', data => post('products', createProduct(data)));


// console.log('Started updating product model');
// customizeProductsModel()
//     .then(() => console.log('Product Model updated!'))
//     .then(() => console.log('Started updating categories model'))
//     .then(() => customizeCategoriesModel())
//     .then(() => console.log('Categories Model updated!'))
//     .then(() => console.log('Started updating monitors model'))
//     .then(() => createMonitorsModel())
//     .then(() => console.log('Monitors Model updated!'))
//     .then(() => console.log('Started updating categories:products model'))
//     .then(() => customizeCategoriesProductsModel())
//     .then(() => console.log('Categories:products Model updated!'));

//TODO: function createBackup() {..}

/*
Логика добавления продуктов в категории:
post('/categories.js/' + category_id + '/products', { product_id: product_id })
 */

/*
Логика загрузки stock_level:
post('/products/' + parent_id + '/stock', {
    variant_id: variant_id,
    parent_id: parent_id,
    quantity: quantity,
    reason: 'received'
}
 */

/*
Логика загрузки размеров:
client.post('/products/' + id + '/variants', {
    name: name,
    active: true,
    price: null,
    shipment_weight: null,
    code: null
}
 */

// import { createProduct } from './factories';

// import { get, post } from './schemaInterface';
// import { types } from './config'


/*
Логика загрузки картинок:
client.post('/products/' + modelsDict.products[obj.id_product] + '/images', {
    //file: { data: { $binary: outData }, content_type: 'image/' + extension },
    file: { url: createUrl(obj.id_image) },
    caption: caption
}
 */

/*
 Загрузка категорий
 import fs from 'fs';
 import csv from 'csv-parser';
 import { createCategory } from './factories';
 import { post } from './schemaInterface';
 fs.createReadStream('./data/categories.csv')
 .pipe(csv())
 .on('data', data => post('categories', createCategory(data)));
 */

/*
 Загрузка производителей
 import fs from 'fs';
 import csv from 'csv-parser';
 import { createManufacturer } from './factories';
 import { post } from './schemaInterface';

 fs.createReadStream('./data/manufacturers.csv')
 .pipe(csv())
 .on('data', data => post('categories', createManufacturer(data)));
 */

/*
 Загрузка продуктов
 import fs from 'fs';
 import csv from 'csv-parser';
 import { createProduct } from './factories';
 import { keyBy } from 'lodash';
 import { get, post } from './schemaInterface';
 import { types } from './config'

 get('categories', {
 type: types.manufacturer,
 limit: -1
 })
 .then(response => response.results)
 .then(manufacturers => keyBy(manufacturers, 'name'))
 .then(manufacturersMap =>
 fs.createReadStream('./data/products.csv')
 .pipe(csv())
 .on('data', data => post('products', createProduct({
 ...data,
 manufacturer_id: manufacturersMap[data.manufacturer_name].id,
 manufacturer_name: data.manufacturer_name
 }))));
 */