/*
Переписать
 */
let customizeProductLayouts = () => {
    // Initializing season-values array
    let seasons = ['AW', 'SS'];
    let years = ['13', '14', '15', '16', '17', '18', '19', '20'];

    let options = [];
    seasons.forEach((season) => {
        years.forEach((year) => {
            options.push({ value: season + year, label: season + year });
        });
    });

    // Customize com.products.new layout
    client.get('/:layouts/com.products.new/sections', {}, (err, res) => {
        let params = res.default;

        // Add 'is_reservation_available'
        params[6].push({
            field: "is_reservation_available",
            type: "input",
            toggle: true,
            inline: true
        });

        // Add 'runway'
        params[6].push({
            field: "runway",
            type: "input",
            toggle: true,
            inline: true
        });

        // Add 'season'
        params.splice(5, 0, [{
            field: 'season',
            type: 'input',
            control: 'select',
            options: options
        }]);

        console.log(params);

        client.put('/:layouts/com.products.new', { $set: { sections: { default: params } } } , (err, res) => {
            console.log('Model com.products.new updated');
            console.log(res);
        });
    });

    // Customize com.products.edit layout
    client.get('/:layouts/com.products.edit/sections', {}, (err, res) => {
        let params = res.default;

        // Add 'is_reservation_available'
        params[7].push({
            field: "is_reservation_available",
            type: "input",
            toggle: true,
            inline: true
        });

        // Add 'runway'
        params[7].push({
            field: "runway",
            type: "input",
            toggle: true,
            inline: true
        });

        // Add 'season'
        params.splice(1, 0, [{
            field: 'season',
            type: 'input',
            control: 'select',
            options: options
        }]);

        console.log(params);
        client.put('/:layouts/com.products.edit', { $set: { sections: { default: params } } }, (err, res) => {
            console.log('Model com.products.edit updated');
            console.log(res);
        });
    });

    // Customize com.products.view layout
    client.get('/:layouts/com.products.view/sections', {}, (err, res) =>{
        let params = res.default;

        // Add 'season'
        params[0][0].fields.push({ field: 'season' });

        // Add 'recommended_products'
        params.push([{
            field: "recommended_products",
            type: "table",
            edit: "recommended",
            columns: [{
                field: "product"
            }]
        }]);

        console.log(params);
        client.put('/:layouts/com.products.view/sections', { default: params } , (err, res) => {
            console.log('Model com.products.view updated');
            console.log(res);
        });
    });

    // Create 'products.recommended' layout
    client.put('/:layouts/products.recommended', {
        model: 'products',
        api: 'com',
        name: "recommended",
        type: "record",
        label: "Recommended Products",
        title: {
            field: "name"
        },
        query: {
            expand: [
                "product"
            ]
        },
        sections: {
            default: [
                [
                    {
                        type: "input",
                        control: "list",
                        field: "recommended_products",
                        fields: [
                            {
                                field: "product",
                                type: "input",
                                lookup: {
                                    fields: [
                                        "name",
                                        "sku"
                                    ]
                                },
                                required: true
                            }
                        ]
                    }
                ]
            ]
        },
        actions: {
            default: [
                {
                    type: "submit"
                }
            ]
        }
    }, (err, res) => {
        console.log('Layout products.recommended created');
        console.log(res);
    });
};