/*
 Переписать
 */
let customizeCategoryLayouts = () => {
    // Customize com.categories.js.new layout
    client.get('/:layouts/com.categories.new/sections', {}, (err, res) => {
        let params = res.default;
        console.log(params);
        params.splice(1, 0, [{
            field: 'type',
            type: 'input',
            control: 'select',
            options: [
                { value: 'Category', label: 'Category' },
                { value: 'Manufacturer', label: 'Manufacturer' }
            ]
        }]);
        console.log(params);
        client.put('/:layouts/com.categories.new/sections', { default: params } , (err, res) => {
            console.log('Model updated');
            console.log(res);
        });
    });

    // Customize com.categories.js.edit layout
    client.get('/:layouts/com.categories.edit/sections', {}, (err, res) => {
        let params = res.default;
        console.log(params);
        params.splice(1, 0, [{
            field: 'type',
            type: 'input',
            control: 'select',
            options: [
                { value: 'Category', label: 'Category' },
                { value: 'Manufacturer', label: 'Manufacturer' }
            ]
        }]);
        console.log(params);
        client.put('/:layouts/com.categories.edit/sections', { default: params } , (err, res) => {
            console.log('Model updated');
            console.log(res);
        });
    });

    // Customize com.categories.js.view layout
    client.get('/:layouts/com.categories.view/sections', {}, (err, res) => {
        let params = res.default;
        console.log(params);
        params[0][0].fields.splice(1, 0, { field: 'type' });
        console.log(params);
        client.put('/:layouts/com.categories.view/sections', { default: params } , (err, res) => {
            console.log('Model updated');
            console.log(res);
        });
    });
};